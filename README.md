We are a multi specialty dental group practice in Upland, Rancho Cucamonga, Chino and Wildomar. We have family dentists, orthodontists, endodontist, periodontist, dental implant specialist, oral surgery and dental anesthesiologist.

Address: 14335 Pipeline Ave, #A, Chino, CA 91710, USA

Phone: 909-902-0800

Website: http://www.myuplanddental.com
